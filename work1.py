from kivy.app import App
from kivy.uix.stacklayout import StackLayout
from kivy.properties import NumericProperty, ObjectProperty

class CalSpace(StackLayout):
    input_1 = ObjectProperty()
    input_2 = ObjectProperty()
    input_3 = ObjectProperty()
    position = []
    check_pos = ["-","-","-","-","-","-","-","-","-"]
    
    def setvalue(self):
        CalSpace.position=[]
        CalSpace.check_pos = ["-","-","-","-","-","-","-","-","-"]
        x = int(self.input_1.text)
        y = int(self.input_2.text)
        z = int(self.input_3.text)
        
        CalSpace.check_pos[x-1] = "0"
        CalSpace.check_pos[y-1] = "0"
        CalSpace.check_pos[z-1] = "0"
        
        CalSpace.position.append(x)
        CalSpace.position.append(y)
        CalSpace.position.append(z)

        CalSpace.position.sort()
        inputall=str(CalSpace.position[0])+" "+str(CalSpace.position[1])+" "+str(CalSpace.position[2])
        print(inputall)
        print(CalSpace.check_pos)

    def calspace(self):
        num1 = CalSpace.position[1]-CalSpace.position[0]-1
        num2 = CalSpace.position[2]-CalSpace.position[1]-1
        if(num2 >= num1 and num2>=1):
            CalSpace.check_pos[CalSpace.position[0]-1] = "-"
            CalSpace.position[0]=CalSpace.position[1]
            CalSpace.position[1]=CalSpace.position[0]+1
            answer=str(CalSpace.position[0])+" "+str(CalSpace.position[1])+" "+str(CalSpace.position[2])
        elif(num1 > num2 and num1>=1):
            CalSpace.check_pos[CalSpace.position[2]-1] = "-"
            CalSpace.position[2]=CalSpace.position[1]
            CalSpace.position[1]=CalSpace.position[2]-1
            answer=str(CalSpace.position[0])+" "+str(CalSpace.position[1])+" "+str(CalSpace.position[2])
        else:
            answer = "None Space!!"
        CalSpace.check_pos[CalSpace.position[0]-1] = "0"
        CalSpace.check_pos[CalSpace.position[1]-1] = "0"
        CalSpace.check_pos[CalSpace.position[2]-1] = "0"
        print(answer)    
        print(CalSpace.check_pos)
        self.textmsg.text = answer
            
class Work1App(App):
    pass

Work1App().run()